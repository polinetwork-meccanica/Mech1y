clear all
close all
clc

%%% Dati

D_e = 230; % [mm] diametro esterno
s = 5.8; % [mm] spessore
p_int = 20; % [MPa] pressione interna

p = 1.5 * p_int; %  [MPa] pressione pulsante massima
N = 12000; % numero cicli richiesti

E = 207000; % [MPa] modulo di Young
v = 0.33; % modulo di Poisson
R_m = 1000; % [MPa] carico a rottura

x = 0:0.01:200;
R = (D_e)/2 - s;

%%% Effetti di bordo

beta = ((3*(1-v^2))^0.25) / (sqrt(s*R));

sigma_x_intradosso = @(beta_x) -(3*p/(4*beta^2*s^2))*exp(-beta_x).*sin(beta_x) + p*R/(2*s);
sigma_x_estradosso = @(beta_x) (3*p/(4*beta^2*s^2))*exp(-beta_x).*sin(beta_x) + p*R/(2*s);

sigma_phi_intradosso = @(beta_x) -(3*v*p/(4*beta^2*s^2))*exp(-beta_x).*sin(beta_x) ...
    - p*R/(4*s)*exp(-beta_x).*cos(beta_x) + p*R/s;
sigma_phi_estradosso = @(beta_x) (3*v*p/(4*beta^2*s^2))*exp(-beta_x).*sin(beta_x) ...
    - p*R/(4*s)*exp(-beta_x).*cos(beta_x) + p*R/s;

figure()
plot(x,sigma_x_intradosso(beta*x))
hold all
plot(x,sigma_x_estradosso(beta*x))
plot(x,sigma_phi_intradosso(beta*x))
plot(x,sigma_phi_estradosso(beta*x))
grid on
legend('sigma_x intradosso','sigma_x estradosso','sigma_{phi} intradosso','sigma_{phi} estradosso','Location','SouthEast')

%%% Verifica con criterio di SIN(E*S)

%%% estradosso

sigma_a_a_intradosso = sigma_x_intradosso(beta.*x)/2;
sigma_a_a_estradosso = sigma_x_estradosso(beta.*x)/2;

sigma_a_m_intradosso = sigma_a_a_intradosso;
sigma_a_m_estradosso = sigma_a_a_estradosso;

sigma_t_a_intradosso = sigma_phi_intradosso(beta.*x)/2;
sigma_t_a_estradosso = sigma_phi_estradosso(beta.*x)/2;

sigma_r_a = p/2;
sigma_r_m = -p/2;

sigma_c_m_intradosso = sigma_t_a_intradosso;
sigma_c_m_estradosso = sigma_t_a_estradosso;

sigma_segnato_a_estradosso = sqrt(sigma_a_a_estradosso.^2 +...
    sigma_t_a_estradosso.^2 - sigma_a_a_estradosso.*sigma_t_a_estradosso);

sigma_a_sines = sigma_segnato_a_estradosso./(1 - (sigma_c_m_estradosso + sigma_a_m_estradosso)/R_m);

%%% intradosso

sigma_segnato_a_intradosso = sqrt(sigma_a_a_intradosso.^2 + sigma_t_a_intradosso.^2 ...
    + sigma_r_a^2 - sigma_a_a_intradosso.*sigma_t_a_intradosso ...
    + sigma_r_a*sigma_t_a_intradosso + sigma_r_a*sigma_t_a_intradosso);

sigma_a_sines_intradosso = sigma_segnato_a_intradosso ./ (1 - ...
    (sigma_c_m_intradosso + sigma_a_m_intradosso + sigma_r_m)/R_m);

figure()
plot(x, sigma_a_sines)
hold all
plot(x, sigma_a_sines_intradosso)
grid on
%legend('sigma sines estradosso','sigma sines intradosso')