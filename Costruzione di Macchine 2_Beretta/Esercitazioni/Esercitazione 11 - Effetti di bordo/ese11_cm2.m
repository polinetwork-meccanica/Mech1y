clc
clear all
close all


% Tutte le acquisizioni esterne di Fede
% eps_x_tot = [-91, 1058.6, -19.5, 25.5, 224]*1e-6;
% eps_phi_tot = [-116.5, 48.5, 566, 823, 939]*1e-6;

eps_x_tot = [- 1058.9, -43, 28, 224]*1e-6;
eps_phi_tot = [ -48.5, 586, 823, 939]*1e-6;

eps_x = eps_x_tot(4);
eps_phi = eps_phi_tot(4);
R = 205;
D = 2*R;
E = 206000;
s = 10;
ni = 0.3;

sigma_x_sperimentale_estradosso = (E/(1 - ni^2))*(eps_x_tot + ni*eps_phi_tot);
sigma_phi_sperimentale_estradosso = (E/(1 - ni^2))*(eps_phi_tot + ni*eps_x_tot);

% figure
% plot(x,sigma_x_sperimentale_estradosso)
% hold all
% plot(x,sigma_phi_sperimentale_estradosso)

% Con fondo indeformabile


P = eps_x*2*E*s/(R*(1 - 2*ni));
P_phi = eps_phi*2*E*s/(R*(2 - ni));
%P = 8;
beta = (3*(1 - ni^2)/(s^2*R^2))^0.25;
D_mant = E*s^3/(12*(1 - ni^2));

w_m = 1/(2*beta^2*D_mant);
w_t = 1/(2*beta^3*D_mant);
theta_m = 1/(beta*D_mant);
theta_t = 1/(2*beta^2*D_mant);

A = [-1/(2*beta^2*D_mant)     1/(2*beta^3*D_mant);
    1/(beta*D_mant) -1/(2*beta^2*D_mant)];
b = [(P*R^2*(2 - ni))/(2*E*s);     0];

M_T = A\b

M =  M_T(1);
T =  M_T(2);



x_estensimetri = [ 22.5, 55, 85, 170];
%x = [0, 0, 55+80, 0, 0];
%plot(x, sigma_x_sperimentale_estradosso, 'o-');
%hold on
%plot(x, sigma_phi_sperimentale_estradosso, 'ro-');
%legend('x', 'phi')
x=0:1:200;
w_mant_m = -M*w_m;
w_mant_t = T*w_t;
w_mant_p = -P*R^2*(2 - ni)/(2*E*s);

%x=0:1:200;
w = 6*(1 - ni^2)/(E*s^3*beta^2)*exp(-beta*x).*(M*(-cos(beta*x)+sin(beta*x))+(T/beta)*cos(beta*x));

M_x = exp(-beta*x).*(M*(sin(beta*x)+cos(beta*x))-T/beta*sin(beta*x));
M_phi = ni*M_x;
T_x = exp(-beta*x).*(-2*beta*M*sin(beta*x)+T*(sin(beta*x)-cos(beta*x)));

sigma_x_estradosso = -6*M_x/(s^2) + P*R/(2*s);
sigma_phi_estradosso = -6*M_phi/(s^2) - E/R*w + P*R/s;

sigma_x_intradosso = 6*M_x/(s^2) + P*R/(2*s);
sigma_phi_intradosso = 6*M_phi/(s^2) - E/R*w + P*R/s;



h = figure()
plot(x_estensimetri, sigma_x_sperimentale_estradosso, 'o-');
hold on
plot(x_estensimetri, sigma_phi_sperimentale_estradosso, 'ro-');
legend('sigma_x', 'sigma_{phi}')
saveas(h,'sforzi sperimentali.png')

%sforzi estradosso fondo incastato
figure()
plot(x, sigma_x_estradosso);
hold all
plot(x, sigma_phi_estradosso);


%sforzi intradosso fondo incastrato
figure(3)
plot(x, sigma_x_intradosso);
hold all
plot(x, sigma_phi_intradosso);

% plot(x_estensimetri, sigma_phi_estradosso);
%legend('x', 'phi')

% Fondo deformabile spesso

%risoluzione del sistema per determinare m e t
spessore_fondo=80;
D_fondo=E*spessore_fondo^3/(12*(1-ni^2));
teta_fondo_m=R/((1+ni)*D_fondo);
teta_fondo_t=s/2*R/((1+ni)*D_fondo);
teta_fondo_p=R^3/8* 1/((1+ni)*D_fondo);


A=[(-1/(2*beta^2*D_mant)-1/(beta*D_mant)*spessore_fondo/2)   (1/(2*beta^3*D_mant)+(1/(2*beta^2*D_mant))*spessore_fondo/2+ R*(1-ni)/(E*spessore_fondo));
    (1/(beta*D_mant)+R/((1+ni)*D_fondo))  (-1/(2*beta^2*D_mant)+ (spessore_fondo/2)*R/((1+ni)*D_fondo)) ];


B=[P*R^2*(2-ni)/(2*E*spessore_fondo);  teta_fondo_p*P];

M_T_fondo_def = A\b

M_fondo_def =  M_T_fondo_def(1);
T_fondo_def =  M_T_fondo_def(2);
%x=[ 22.5, 55, 85, 170];
w_fondo_def = 6*(1 - ni^2)/(E*s^3*beta^2)*exp(-beta*x).*(M_fondo_def*(-cos(beta*x)+sin(beta*x))+(T_fondo_def/beta)*cos(beta*x))-T_fondo_def*R*(1-ni)/(E*s);
M_x_fondo_def = exp(-beta*x).*(M_fondo_def*(sin(beta*x)+cos(beta*x))-T_fondo_def/beta*sin(beta*x));
M_phi_fondo_def = ni*M_x_fondo_def;
T_x_fondo_def = exp(-beta*x).*(-2*beta*M_fondo_def*sin(beta*x)+T_fondo_def*(sin(beta*x)-cos(beta*x)));

sigma_x_estradosso_fondo_def = -6*M_x_fondo_def/(s^2) + P*R/(2*s);
sigma_phi_estradosso_fondo_def = -6*M_phi_fondo_def/(s^2) - E/R*w + P*R/s;

sigma_x_intradosso_fondo_def = 6*M_x_fondo_def/(s^2) + P*R/(2*s);
sigma_phi_intradosso_fondo_def = 6*M_phi_fondo_def/(s^2) - E/R*w + P*R/s;



%sforzi estradosso fondo incastato
figure(6)
plot(x, sigma_x_estradosso_fondo_def);
hold all
plot(x, sigma_phi_estradosso_fondo_def);


%sforzi intradosso fondo incastrato
figure(7)
plot(x, sigma_x_intradosso_fondo_def);
hold all
plot(x, sigma_phi_intradosso_fondo_def);

% plot(x_estensimetri, sigma_phi_estradosso);
%legend('x', 'phi')
%sigma_x_estradosso_fondo_spesso=
