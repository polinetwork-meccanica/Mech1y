clear all
close all
clc


%%%% carico file esempio fischi

[nome cart]=uigetfile();
load([cart,nome])

sens=30.3; %mV/Pa
p_ref=2e-5;

microfono=Dati(:,2);

microfono=microfono*1000/sens;

x=microfono;

fs=5210;
fsamp=5210;

t=[0:1/fs:(length(x)-1)/fs];

% x=1.41*sin(2*pi*t*2000);

%%%%% rms su tutta la storia temporale -> poco significativo

rms_tot=sqrt(sum(x.^2)/length(x));

rms_tot_db=20*log10(rms_tot/p_ref);

disp(rms_tot_db)


%%%% rms su 1 s partendo da t=0 e da t=0.5 s  -> differenza risultati

rms_1s_0=[];
rms_1s_0_5=[];

t_1s_0=[];
t_1s_0_5=[];


for ttt=1:fsamp:length(x)-fsamp
    
    rms_1s_0=[rms_1s_0,sqrt(sum(x(ttt:ttt+fsamp-1).^2)/fsamp)];
    t_1s_0=[t_1s_0,(ttt+fsamp)/fsamp];

    rms_1s_0_5=[rms_1s_0_5,sqrt(sum(x(ttt+fsamp/2:ttt+fsamp+fsamp/2-1).^2)/fsamp)];
    t_1s_0_5=[t_1s_0_5,(ttt+fsamp+fsamp/2)/fsamp];
    
end

rms_1s_0_db=20*log10(rms_1s_0/p_ref);
rms_1s_0_5_db=20*log10(rms_1s_0_5/p_ref);


%%%% rms su 1 s con overlap (1 campione) -> risultato univoco

rms_1s=[];

for ttt=1:length(x)-fsamp
 
rms_1s=[rms_1s,sqrt(sum(x(ttt:ttt+fsamp-1).^2)/fsamp)];

end

t_1s=t(fsamp+1:length(rms_1s)+fsamp);
rms_1s_db=20*log10(rms_1s/p_ref);

figure
set(gca,'fontsize',16)
grid on
hold on
plot(t_1s_0,rms_1s_0_db,t_1s_0_5,rms_1s_0_5_db,t_1s,rms_1s_db)
xlabel('tempo [s]','fontsize',18)
ylabel('dB','fontsize',18)
legend('rms 1 s - t(0) = 0 s','rms 1 s - t(0) = 0.5 s','rms 1 s - overlap 1 campione')

%%% rms con finestra fast e slow

tau_fast=1/8;
tau_slow=1;

fc_fast=1/(2*pi*tau_fast);
fc_slow=1/(2*pi*tau_slow);


[b_fast,a_fast]=butter(1,fc_fast/(fs/2));

[b_slow,a_slow]=butter(1,fc_slow/(fs/2));


[rms_fast]=filter(b_fast,a_fast,x.^2);
[rms_slow]=filter(b_slow,a_slow,x.^2);

rms_fast=sqrt(rms_fast);
rms_slow=sqrt(rms_slow);


rms_fast_db=20*log10(rms_fast/p_ref);
rms_slow_db=20*log10(rms_slow/p_ref);



figure
set(gca,'fontsize',16)
grid on
hold on
plot(t,microfono,t,rms_fast,t,rms_slow)
xlabel('tempo [s]','fontsize',18)
ylabel('Pressure [Pa]','fontsize',18)
legend('microfono','rms - fast','rms - slow')


figure
set(gca,'fontsize',16)
grid on
hold on
plot(t,rms_fast_db,t,rms_slow_db)
xlabel('tempo [s]','fontsize',18)
ylabel('dB','fontsize',18)

legend('rms - fast','rms - slow')

