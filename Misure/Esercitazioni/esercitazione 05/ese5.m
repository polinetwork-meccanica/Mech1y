clear all 
close all
clc

[nome cart]=uigetfile();
load([cart,nome])

sens_martello=2.361/1000;
sens_acc1=97.5/1000/9.81;
sens_acc2=101.6/1000/9.81;

fsamp=10240;

martello=Dati(:,1)/sens_martello;
acc1=Dati(:,2)/sens_acc1;
acc2=Dati(:,3)/sens_acc2;

Dati_taglio_mart=[];
Dati_taglio_acc1=[];
Dati_taglio_acc2=[];

figure
plot(martello)
int=ginput();

T=8;

for ttt=1:2:length(int)
    
    int=floor(int);
    
    [a,b]=max(martello(int(ttt,1):int(ttt+1,1)));
    b=floor(b+(int(ttt,1)));
    
    Dati_taglio_mart=[Dati_taglio_mart,martello(b-0.2*fsamp:b+(T-0.2)*fsamp)];
    Dati_taglio_acc1=[Dati_taglio_acc1,acc1(b-0.2*fsamp:b+(T-0.2)*fsamp)];
    Dati_taglio_acc2=[Dati_taglio_acc2,acc2(b-0.2*fsamp:b+(T-0.2)*fsamp)];

end

dt=1/fsamp;
tempo=[0:length(Dati_taglio_mart)-1]*dt;

figure
set(gca,'fontsize',16)

plot(tempo,Dati_taglio_mart)
title('MARTELLO','fontsize',20)
xlabel('tempo [s]','fontsize',20)
ylabel('forza [N]','fontsize',20)

figure
set(gca,'fontsize',16)

plot(tempo,Dati_taglio_acc1)
title('ACCELEROMETRO 1','fontsize',20)
xlabel('tempo [s]','fontsize',20)
ylabel('accelerazione [m/s^2]','fontsize',20)

FRF1=[];
FRF2=[];

for ggg=1:size(Dati_taglio_mart,2)
    
    [spettro_acc1, freq]=fft_norm(Dati_taglio_acc1(:,ggg),fsamp);
      [spettro_acc2, freq]=fft_norm(Dati_taglio_acc2(:,ggg),fsamp);
      [spettro_mart, freq]=fft_norm(Dati_taglio_mart(:,ggg),fsamp);

    FRF_A1=spettro_acc1./spettro_mart;
    FRF_A2=spettro_acc2./spettro_mart;

    FRF1=[FRF1,FRF_A1];

    FRF2=[FRF2,FRF_A2];

end

figure
set(gca,'fontsize',20)
semilogy(freq,abs(FRF1))

hold on
semilogy(freq,abs(mean(FRF1')),'y','linewidth',2)
title('ACCELEROMETRO 1 - FRF singole martellate e media FRF','fontsize',20)
xlabel('frequenze [Hz]','fontsize',20)
ylabel('|FRF| [N/ms^-^2]','fontsize',20)


figure
set(gca,'fontsize',20)
semilogy(freq,abs(FRF2))

hold on
semilogy(freq,abs(mean(FRF2')),'y','linewidth',2)
title('ACCELEROMETRO 2 - FRF singole martellate e media FRF','fontsize',20)
xlabel('frequenze [Hz]','fontsize',20)
ylabel('|FRF| [N/ms^-^2]','fontsize',20)


gxx=0;
gxy=0;
gyx=0;
gyy=0;

for ggg=1:size(Dati_taglio_mart,2)
    
[spettro_acc1, frequenze]=fft_norm(Dati_taglio_acc1(:,ggg),fsamp);
[spettro_mart, frequenze]=fft_norm(Dati_taglio_mart(:,ggg),fsamp);

gxx=gxx+(conj(spettro_mart).*spettro_mart)/2;
gxy=gxy+(conj(spettro_mart).*spettro_acc1)/2;
gyx=gyx+(conj(spettro_acc1).*spettro_mart)/2;
gyy=gyy+(conj(spettro_acc1).*spettro_acc1)/2;

end

gxx=gxx/ggg;
gxy=gxy/ggg;
gyx=gyx/ggg;
gyy=gyy/ggg;

acca1_1=gxy./gxx;
acca2_1=gyy./gyx;


gxx=0;
gxy=0;
gyx=0;
gyy=0;

for ggg=1:size(Dati_taglio_mart,2)
    
[spettro_acc2, frequenze]=fft_norm(Dati_taglio_acc2(:,ggg),fsamp);
[spettro_mart, frequenze]=fft_norm(Dati_taglio_mart(:,ggg),fsamp);

gxx=gxx+(conj(spettro_mart).*spettro_mart)/2;
gxy=gxy+(conj(spettro_mart).*spettro_acc2)/2;
gyx=gyx+(conj(spettro_acc2).*spettro_mart)/2;
gyy=gyy+(conj(spettro_acc2).*spettro_acc2)/2;

end

gxx=gxx/ggg;
gxy=gxy/ggg;
gyx=gyx/ggg;
gyy=gyy/ggg;

acca1_2=gxy./gxx;
acca2_2=gyy./gyx;

figure
set(gca,'fontsize',20)
semilogy(frequenze,abs(mean(FRF1')),'k','linewidth',2)
title('ACCELEROMETRO 1 - media FRF e stimatori H1, H2','fontsize',20)
hold on
semilogy(frequenze,abs(acca1_1),'r')
semilogy(frequenze,abs(acca2_1),'g')
legend('media FRF','H1','H2')

xlabel('frequenze [Hz]','fontsize',20)
ylabel('|FRF| [N/ms^-^2]','fontsize',20)


figure
set(gca,'fontsize',20)
semilogy(frequenze,abs(mean(FRF2')),'k','linewidth',2)
title('ACCELEROMETRO 2 - media FRF e stimatori H1, H2','fontsize',20)
hold on
semilogy(frequenze,abs(acca1_2),'r')
semilogy(frequenze,abs(acca2_2),'g')
legend('media FRF','H1','H2')

xlabel('frequenze [Hz]','fontsize',20)
ylabel('|FRF| [N/ms^-^2]','fontsize',20)
