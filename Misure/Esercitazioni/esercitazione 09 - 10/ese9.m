clear all
close all
clc


load('dati_scala.mat')

fsamp=128;
accelerazione=accelerazione-mean(accelerazione);

%%%%% ho a disposizione i dati di forzamento (forza) e di risposta (accelerazione)
%%%%% in un punto della struttura

%%%%% utilizzo questi dati per stimare la funzione di risposta in
%%%%% frequenza(H, H1, H2)


sxx=0;
sxy=0;
syx=0;
syy=0;
H=0;


T=120; %%% definisco il tempo di osservazione in base alla risoluzione in frequenza richiesta (df=1/T)
n_pt=T*fsamp; %%% definisco il numero di punti in ogni intervallo

n_int=floor(length(forza)/(n_pt));  %%% definisco il numero di intervalli disponibili in base alla lunghezza dei dati


for ggg=1:n_int
    
    %%% per ogni intervallo di dati calcolo la trasformata di fourier di accelerazione e forzante    
    
[spettro_acc]=fft(accelerazione((ggg-1)*n_pt+1:ggg*n_pt))/n_pt; 
[spettro_forza]=fft(forza((ggg-1)*n_pt+1:ggg*n_pt))/n_pt;

%%%% in ogni intervallo stimo H, gli auto e e i cross-spettri e li sommo in
%%%% un vettore
H=H+spettro_acc./spettro_forza; 
sxx=sxx+(conj(spettro_forza).*spettro_forza);
sxy=sxy+(conj(spettro_forza).*spettro_acc);
syx=syx+(conj(spettro_acc).*spettro_forza);
syy=syy+(conj(spettro_acc).*spettro_acc);

end

%%% divido H, gli auto e i cross-spettri per il numero di intervalli
%%% considerati per ottenere le mederime quantit� mediate

sxx=sxx/ggg;
sxy=sxy/ggg;
syx=syx/ggg;
syy=syy/ggg;

H=H/ggg;

%%% stimo H1 e H2 utlizzando gli auto e cross spettri medi

H1=sxy./sxx;
H2=syy./syx;

% Coerenza
coerenza=(sxy.*conj(sxy))./(sxx.*syy);


%%% definisco il vettore delle frequenze positive ed estraggo le FRF per
%%% frequenze positiva

frequenze=[0:1/T:fsamp/2];
H_aux=H(1:length(frequenze));
H1_aux=H1(1:length(frequenze));
H2_aux=H2(1:length(frequenze));

%%%% grafico i risultati


%%% autospettri medi di ingresso e uscita

figure (1)
subplot(2,1,1)
set(gca,'fontsize',20)
semilogy(frequenze,2*sxx(1:length(frequenze)),'linewidth',2)
xlim([3 25]);
ylim([10e-6,1])
ylabel('gxx')
subplot(2,1,2)
set(gca,'fontsize',20)
semilogy(frequenze,2*syy(1:length(frequenze)),'linewidth',2)
ylabel('gyy')
xlabel('frequency [Hz]')
xlim([3 25]);

%%% FRF (modulo e fase) - gli stimatori H1 e H2 permettono di migliorare la
%%% stima delle FRF e di ridurre l'effetto del rumore

figure(2)

subplot(2,1,1)
set(gca,'fontsize',20)
semilogy(frequenze,abs(H_aux),frequenze,abs(H1_aux),frequenze,abs(H2_aux),'linewidth',2)
xlim([3 25]);
ylim([10e-6,1])
ylabel('|FRF| [m/s^2/N]')
subplot(2,1,2)
set(gca,'fontsize',20)

plot(frequenze,angle(H_aux)*180/pi,frequenze,angle(H1_aux)*180/pi,frequenze,angle(H2_aux)*180/pi,'linewidth',2)
legend('H','H1','H2')
ylabel('\angle{FRF} [deg]')
xlabel('frequency [Hz]')

xlim([3 25]);

%%% |FRF| e coerenza - la coerenza risulta essere elevata nella banda di
%%% frequenze in cui la struttura viene forzata (gli abbassamenti di
%%% coerenza nella banda 7-20 Hz  si verificano in corrispondenza delle
%%% antirisonanze della struttura, dove la risposta del sistema � molto
%%% piccola e di conseguenza il rapporto segnale rumore � molto basso. La
%%% FRF � quindi stimata correttamente nella banda 7-20 Hz, dove si
%%% osservano le prime risonanze della struttura.

figure(3)
subplot(2,1,1)
set(gca,'fontsize',20)

semilogy(frequenze,abs(H_aux),frequenze,abs(H1_aux),frequenze,abs(H2_aux),'linewidth',2)
xlim([3 25]);
ylabel('|FRF| [m/s^2/N]')

subplot(2,1,2)
set(gca,'fontsize',20)

plot(frequenze,coerenza(1:length(frequenze)),'linewidth',2);
xlim([3 25]);
ylabel('\gamma^2')
xlabel('frequency [Hz]')


