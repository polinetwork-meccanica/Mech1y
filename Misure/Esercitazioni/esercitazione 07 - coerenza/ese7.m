clear all
close all
clc


[nome,cart]=uigetfile();

load([cart,nome]);

% plot(Dati(:,1))
% 
% lim=ginput();
% 
% Dati=Dati(floor(lim(1,1)):floor(lim(2,1)),:);


fsamp=5120;

dt=1/fsamp;
N=length(Dati);
tempo=0:dt:(N-1)*dt;
delta_f=0.5;

%%% Finestra analisi [s]
finestra=1/delta_f;
nfft=floor(finestra*fsamp);
over=0/100;

wind=ones(nfft,1);

%%

% Calcolo Gxy Gxx Gyy correttamente
[gxy , ~, freqsp]=autocross(Dati(:,1),Dati(:,2),fsamp,nfft,over,wind);
[gxx , ~, ~]=autocross(Dati(:,1),Dati(:,1),fsamp,nfft,over,wind);
[gyy , ~, ~]=autocross(Dati(:,2),Dati(:,2),fsamp,nfft,over,wind);


% Determino le risoluzioni in frequenza
df=freqsp(2)-freqsp(1);

% Coerenza
coerenza=(gxy.*conj(gxy))./(gxx.*gyy);

%%

figure
plot(tempo,Dati)
set(gca,'fontsize',14)
xlabel('[s]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
semilogy(freqsp,gxx)
title('Auto-spettro accelerometro')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[(m/s^2^2)]','fontsize',14)
grid

figure
semilogy(freqsp,gyy)
title('Auto-spettro microfono')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[(m/s^2^2)]','fontsize',14)
grid


figure
semilogy(freqsp,abs(gxy))
title('Cross-spettro')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[(m/s^2^2)]','fontsize',14)
grid

figure
plot(freqsp,coerenza)
title('Coerenza')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
grid
% 

figure
plot(freqsp,gyy,freqsp,coerenza.*gyy,'linewidth',2)
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[(m/s^2^2)]','fontsize',14)
legend('autospettro microfono','potenza coerente microfono')
grid

xlim([1 1000])

