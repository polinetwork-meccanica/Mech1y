% Funzione autospettro e cross spettro:
% Rende in uscita l'auto/cross spettro mediato, finestrato e con overlap in
% EU^2
%
% [uscita,mediacomp,frequenze]=chiamami(dati1,dati2,fsamp,lungfin,overlap,finestra);
% 
% dati1, dati2: storie temporali in ingresso
% fsamp: frequenza di campionamento
% lungfin: lunghezza del sottorecord in cui dividere la storia temporale
% overlap: punti di overlap
% finestra: finestra temporale usata per pesare i dati


function [autocross_mean,spettro_mean,frequenze]=autocross(dati1,dati2,fsamp,lungfin,overlap,finestra);

a=size(dati1);
b=size(dati2);

if a(2)>a(1)
    dati1=dati1';
end

if b(2)>b(1)
    dati2=dati2';
end


N=length(dati1);
df=fsamp/lungfin;

if (lungfin/2)==(floor(lungfin/2))
    frequenze=0:df:(lungfin/2*df);test=1;
else
    frequenze=0:df:((lungfin-1)/2)*df;test=0;
end

NF=length(frequenze);

in=1;
puntofin=0;
somma_complex=zeros(NF,1);
autocross=zeros(NF,1);

while puntofin < N

    inizio=(in-1)*(lungfin-overlap)+1;
    
    fine=inizio+(lungfin-1);
    
    spettro1=fft_norm(finestra.*dati1(inizio:fine),fsamp);
    
    somma_complex=somma_complex+spettro1;
    
    spettro2=fft_norm(finestra.*dati2(inizio:fine),fsamp);
    
    autocross=autocross+conj(spettro1).*spettro2/2;
       
in=in+1;
puntofin=fine+lungfin;

end

spettro_mean=somma_complex/(in-1);
autocross_mean=autocross/(in-1);

