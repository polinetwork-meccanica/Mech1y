clear all
close all
clc

[fnome percorso]=uigetfile('*.mat','Che apriamo?');
load([percorso fnome]);

dt=1/fsamp;
N=length(Dati);
tempo=0:dt:(N-1)*dt;

prompt={'Finestra analisi [s]','Over %'};
def={'30','66'};
risp=inputdlg(prompt,'Parametri spettro',1,def);
nfft=str2double(char(risp(1)))*fsamp;
over=nfft*str2double(char(risp(2)))/100;

wind=hanning(nfft);

%%

% Calcolo Gxy Gxx Gyy correttamente
[gxy , ~, freqsp]=autocross(Dati(:,1),Dati(:,2),fsamp,nfft,over,wind);
[gxx , ~, ~]=autocross(Dati(:,1),Dati(:,1),fsamp,nfft,over,wind);
[gyy , ~, ~]=autocross(Dati(:,2),Dati(:,2),fsamp,nfft,over,wind);

% Calcolo Gxy su una finestra di un solo secondo
[gxy_1s , ~ , freqsp_1s]=autocross(Dati(1:nfft,1),Dati(1:nfft,2),fsamp,nfft,0,wind);

% Calcolo Gxy su tutta la storia temporale senza mediare
[gxy_tutto delme freqsp_tutto]=autocross(Dati(:,1),Dati(:,2),fsamp,length(Dati),0,rectwin(length(Dati)));

% Determino le risoluzioni in frequenza
df=freqsp(2)-freqsp(1);
df_tutto=freqsp_tutto(2)-freqsp_tutto(1);
df_1s=freqsp_1s(2)-freqsp_1s(1);

% Coerenza
coerenza=(gxy.*conj(gxy))./(gxx.*gyy);

%%

figure
plot(tempo,Dati)
set(gca,'fontsize',14)
xlabel('[s]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
semilogy(freqsp,gxx)
title('Auto-spettro ingresso')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
semilogy(freqsp,gyy)
title('Auto-spettro uscita')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
semilogy(freqsp_tutto,abs(gxy_tutto))
title('Cross-spettro uscita tutto il segnale')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
semilogy(freqsp_1s,abs(gxy_1s))
title(['Cross-spettro uscita (' char(risp(1)) 's)'])
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
semilogy(freqsp,abs(gxy))
title('Cross-spettro medio uscita')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
ylabel('[m/s^2]','fontsize',14)
grid

figure
plot(freqsp,coerenza)
title('Coerenza')
set(gca,'fontsize',14)
xlabel('[Hz]','fontsize',14)
grid