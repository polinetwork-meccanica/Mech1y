clear all; close all; clc;

[nome, cartella] = uigetfile('multiselect', 'on');

cd(cartella);

for t = 1:11
    load(char(nome(t)));
    N=length(y);
    df=fs/length(y);
    dt=1/fs;
    freq=0:dt:(N/2-1)*dt
    Y=fft(y);
    plot(t,Y(1:length(Y)/2))
    stem(freq,abs(Y(1:length(Y)/2)))
    pause()
end

% solo nel primo e nell'ultimo caso non abbiamo leakage perch� il periodo rimane intero