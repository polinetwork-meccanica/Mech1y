clear all
close all
clc

%% Carico i file

[nomefile,cartella]=uigetfile('*.mat','Carica file');
load([cartella nomefile]);

%% Plotto le storie temporali

y=Dati(:,1)';
y2=Dati(:,2)';

dt=1/fsamp;
N=length(y);
t=0:dt:(N-1)*dt;

figure
plot(t,Dati)
xlabel('tempo [s]')
grid

%% Calcolo la fft

k_var=N/2;

trasf1=zeros(N+1,1);
trasf2=zeros(N+1,1);

for k=-k_var:k_var
    
    sommatoria=exp(-1j*2*pi*(k).*(0:N-1)./N);
    termine1=sum(y.*sommatoria)./N;
    trasf1(k+k_var+1)=termine1;
      termine2=sum(y2.*sommatoria)./N;
    trasf2(k+k_var+1)=termine2;
end

T=N*dt;
df=1/T;
freq=-k_var.*df:df:(k_var)*df;

freq=[0:df:(N/2)*df];
trasf2=2*trasf2(end/2:end/2+N/2);
trasf2(1)=trasf2(1)/2;
trasf1=2*trasf1(end/2:end/2+N/2);
trasf1(1)=trasf1(1)/2;

figure
subplot(2,1,1)
plot(freq,abs(trasf1))
ylabel('modulo spettro canale 1')
grid on

subplot(2,1,2)
plot(freq,abs(trasf2))
xlabel('freq [Hz]')
ylabel('modulo spettro canale 2')
title('fft normalizzata')
grid

% %% Plotto lo spettro per le sole frequenze >= 0
% 
% freq2=[0:df:(N/2)*df];
% trasf2=2*trasf(end/2:end/2+N/2);
% trasf2(1)=trasf2(1)/2;
% 
% figure
% subplot(2,1,1)
% plot(freq2,abs(trasf2))
% xlabel('freq [Hz]')
% ylabel('modulo')
% subplot(2,1,2)
% plot(freq2,angle(trasf2))
% xlabel('freq [Hz]')
% ylabel('fase spettro')
% title('fft normalizzata')
% grid
% 
% %% Nyquist
% 
% figure
% plot(real(trasf),imag(trasf))
% xlabel('Re')
% ylabel('Im')
% title('Nyquist')
% grid
% 
% %% Parte reale e parte immagnaria in funzione della frequenza
% 
% figure
% subplot(2,1,1),plot(freq,real(trasf)),xlabel('freq [Hz]'),ylabel('parte reale')
% subplot(2,1,2),plot(freq,imag(trasf)),xlabel('freq [Hz]'),ylabel('parte immaginaria')
% 
% 
% 
