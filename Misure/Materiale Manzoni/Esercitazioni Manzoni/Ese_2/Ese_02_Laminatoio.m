%calcola rms e media del segnale scelto secondo la definizione
%calcola e plotta rms e media viaggiante (l'rms � depurato del valor medio sul singolo intervallo di tempo)

clear all
close all
clc
%% 

%Carico i dati

[nome path]=uigetfile('*.mat','scegli il file');
load([path,nome]);
[r c]=size(Dati);

%Converto i segnali in tensione in grandezze fisiche
for jj=1:c
    
    Forza(:,jj)=Dati(:,jj)/sens(jj);
        
end

% Calcolo l'asse dei tempi
dt=1/fsamp;
t=[0:dt:dt*(r-1)]';

figure
for kk=1:c
    subplot(2,2,kk)
    plot(t,Forza(:,kk))
    grid on
    axis tight
    xlabel('t [s]','Fontsize',12)
    ylabel('Forza [kN]','Fontsize',12)
    title(['colonna ' num2str(kk)],'Fontsize',12)
end

pause

for jj=1:c
    
    t_iniz=3;
    media_iniz(jj)=mean(Forza(1:t_iniz/dt,jj));
    Forza(:,jj)=Forza(:,jj)-media_iniz(jj);
    
end

ch=input('Quale canale vuoi analizzare? ');

% Plotto le storie temporali
figure
plot(t,Forza(:,ch))
xlabel('tempo [s]','Fontsize',15)
ylabel('Forza [kN]','Fontsize',15)
grid on
axis tight


%%

% Calcolo gli indici media ed rms di tutta la storia temporale
media=mean(Forza(:,ch));
devstd=std(Forza(:,ch));
rms=sqrt(mean(Forza(:,ch))^2+std(Forza(:,ch))^2);

%%

it=input('inserisci intervallo temporale su cui calcolare media ed rms viaggianti [s]: ');

% Calcolo le finestre viaggianti
N=it*fsamp;
ngruppi=floor(length(t)/N);

mediaf=zeros(1,ngruppi);
rmsf=zeros(1,ngruppi);

rrw=ones(1,ngruppi);

for ii=1:ngruppi
    
    % Osservo il segnale sulla finestra
    sig_pezzo=Forza((ii-1)*N+1:N*ii,ch);
    
    % Calcolo la media
    mf=mean(sig_pezzo);

%     % Depuro la media dal segnale
%     sigsenzamed=sig_pezzo-mean(sig_pezzo);
    
    % Registro la media della finestra temporale corrente nel vettore delle
    % medie viaggianti
    mediaf(ii)=mf;

%     % Calcolo la media del segnale depurato dalla media (deve essere zero)
%     mf2=mean(sigsenzamed);

%     % Calcolo la dev st del segnale depurato dalla media
%     dev_stf(ii)=std(sigsenzamed);
% 
%     % Calcolo l'rms del segnale depurato dalla media
%     rmsf(ii)=sqrt(mf2^2+dev_stf(ii)^2);

    % Calcolo la deviazione standard della porzione di segnale
    std_f(ii)=std(sig_pezzo);
%     
%     % Calcolo l'rms del segnale totale
%     rmsf2(ii)=sqrt(mf^2+dev_stf(ii)^2);

    % Calcolo l'rms del segnale totale
    rmsf(ii)=sqrt(mf^2+std_f(ii)^2);
    
    % Calcolo il vettore tempo a cui si riferiscono gli indici
    rrw(ii)=rrw(ii)*ii;
    
end

temporms=rrw*it;

%%

% Plotto rms e media viaggianti
figure

subplot(3,1,1)
plot(temporms,mediaf','LineWidth',2)
xlabel('tempo [s]','Fontsize',15)
ylabel('\mu','Fontsize',15)
line([temporms(1) temporms(end)],[media media],'LineStyle','--')
grid on
axis tight

subplot(3,1,2)
plot(temporms,std_f,'r','LineWidth',2)
xlabel('tempo [s]','Fontsize',15)
ylabel('\sigma','Fontsize',15)
line([temporms(1) temporms(end)],[devstd devstd],'LineStyle','--','color','r')
grid on
axis tight

subplot(3,1,3)
plot(temporms,rmsf','c','LineWidth',2)
xlabel('tempo [s]','Fontsize',15)
ylabel('\psi','Fontsize',15)
line([temporms(1) temporms(end)],[rms rms],'LineStyle','--','color','c')
grid on
axis tight

