clear all
% close all
clc

%% Carico i dati

[n p]=uigetfile('.mat', 'seleziona i dati');
cd(p)
load(n);

StoriaTemp1=StoriaTemp(:,1)./sens;
StoriaTemp2=StoriaTemp(:,2)./sens;

% Costruzione asse dei tempi
Ntot=length(StoriaTemp1);
dt=1/fsamp;
ttot=0:dt:(Ntot/fsamp)-dt;

% Grafico dei segnali in funzione del tempo
figure
set(gca, 'fontsize', 16)
plot(ttot,StoriaTemp1,'b')
hold on
plot(ttot,StoriaTemp2,'r')
xlabel('t [s]')
ylabel('spostamento [mm]')
title('Storie temporali')
legend('Laser 1','Laser 2')
grid on

% Lavoro su 1/4 dei dati per snellire la procedura
dati1=StoriaTemp(1:end/4,1)./sens;
dati2=StoriaTemp(1:end/4,2)./sens;

N=length(dati1);
dt=1/fsamp;
t=0:dt:(N/fsamp)-dt;


%% Crosscorrelazione ciclo for

for kk=-N+1:N-1
    
    if kk>=0
    x=dati1(1:end-kk);
    y=dati2(kk+1:end);
    corr(N+kk)=sum(x.*y)./(N-abs(kk));
    else
    x=dati1(abs(kk)+1:end);
    y=dati2(1:end-abs(kk));
    corr(kk+(N-1)+1)=sum(x.*y)./(N-abs(kk));
    end
    
end

tau=(-N+1)*dt:dt:(N-1)*dt;
figure(30)
set(gca, 'fontsize', 16)
plot(tau,corr)
xlabel('tau [s]')
ylabel('R_x_y [mm^2]')
grid on
axis tight
title('Crosscorrelazione')
set(gca, 'fontsize', 16)
        
%% Crosscorrelazione xcorr

[corr_mat,lag]=xcorr(dati2,dati1,'unbiased');

%% Calcolo velocit�

distanza=18/1000;

[Y_m,max_matl]=max(corr_mat(1:end-0.5*fsamp));
delay_mat=abs(lag(max_matl)*dt);

[Y,max_c]=max(corr(1:end-0.5*fsamp));
delay=abs(tau(max_c));
figure(30)
set(gca, 'fontsize', 16)
hold on
plot(tau(max_c),Y,'or')

vel_m=distanza/delay_mat;
vel=distanza/delay;

disp(['La velocit� di avanzamento � ' num2str(vel) ' m/s'])

%% Segnali in funzione dello spazio

spazio=ttot.*vel;
figure
set(gca, 'fontsize', 16)
plot(spazio,StoriaTemp1,spazio,StoriaTemp2)
xlabel('Spazio [m]')
ylabel('Spostamento [mm]')
title('Storie temporali')
legend('Laser 1','Laser 2')

axis tight
grid on


%% Rms viaggiante ogni metro

dati_nomedia=(StoriaTemp(:,1)-mean(StoriaTemp(:,1)))./sens;
dati_nomedia2=(StoriaTemp(:,2)-mean(StoriaTemp(:,2)))./sens;
n_punti=round(1/vel*fsamp);
n_storie=floor(length(StoriaTemp(:,1))/n_punti);

for ii=1:n_storie
    d=dati_nomedia((ii-1)*n_punti+1:n_punti*ii);
    d2=dati_nomedia2((ii-1)*n_punti+1:n_punti*ii);
    rms(ii)=sqrt(std(d)^2);
    rms2(ii)=sqrt(std(d2)^2);
end

figure
set(gca, 'fontsize', 16)
plot(rms,'-ob')
hold on
plot(rms2,'-or')
xlabel('Distanza [m]')
ylabel('RMS [mm]')
grid on
legend('Laser 1','Laser 2')










        