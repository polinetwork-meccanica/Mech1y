%Questo programma calcola media, max, min, deviazione standard e rms per un
%numero variabile di segnali.
%I segnali devono essere passati in una matrice e devono essere disposti
%per colonne

clear all
close all
clc

%% carico il file
[nome path]=uigetfile('*.mat','scegli il file da aprire');
cd(path);
load(nome);

%% taratura dei segnali

[r c]=size(Dati);
Dati(:,1)=Dati(:,1)./sens(1);
Dati(:,2)=Dati(:,2)./sens(2);

%% grafico dei segnali

dt=1/fsamp;

tempo1=zeros(r,1);
for u=1:1:r
    tempo1(u)=(u-1)*dt;
end

tempo=dt:dt:r*dt;

%plotto i canali
figure(1)
for gg=1:1:c
    subplot(c,1,gg)
    plot(tempo,Dati(:,gg));
    xlabel('tempo [s]')
    ylabel('Accelerazione [m/s^2]')
    legend(char(canali(gg)));
    grid on
end

%% analisi statistica di base

massimo=zeros(1,c);
massimo2=zeros(1,c);
minimo=zeros(1,c);
media=zeros(1,c);
dev_st=zeros(1,c);
rms=zeros(1,c);
rms2=zeros(1,c);

%ciclo sui vari segnali (colonne)
for i=1:1:c
    
    massimo(i)=max(Dati(:,i));
    minimo(i)=min(Dati(:,i));
    media(i)=mean(Dati(:,i));
    dev_st(i)=std(Dati(:,i));
    
    rms(i)=sqrt(media(i)^2+dev_st(i)^2);
    rms2(i)=sqrt(sum((Dati(:,i).^2).*dt)/(r*dt));
    
    %calcolo il massimo per ricorsione
    rif=Dati(1,i);
    
    for t=1:1:r
        hk=Dati(t,i);
        if hk>rif
            rif=hk;
        end
    end
    
    massimo2(i)=rif;   
    
end

%verifico l'uguaglianza di massimo e massimo2 per il primo canale
    
if massimo(1)==massimo2(1)
    disp('i due valori sono uguali')
else
    disp('ERRORE')
end

%% plotto i segnali e i valori statistici

%plotto i canali
for gg=1:1:c
    figure()
    plot(tempo,Dati(:,gg));
    line([0 tempo(end)],[minimo(gg) minimo(gg)],'Linewidth',2,'color','g')
    line([0 tempo(end)],[massimo(gg) massimo(gg)],'Linewidth',2,'color','m')
    line([0 tempo(end)],[rms(gg) rms(gg)],'Linewidth',4,'color','y')
    xlabel('tempo [s]')
    ylabel('Accelerazione [m/s^2]')
    legend(char(canali(gg)),'minimo','massimo','rms');
    grid on
end

%% salvataggio file
    
save('stat.txt','-ascii','media','minimo','massimo','dev_st','rms');