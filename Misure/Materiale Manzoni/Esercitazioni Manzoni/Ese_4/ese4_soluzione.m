clear all
close all
clc

%% carico il file

[f p]=uigetfile('*.mat','apri file');
old=cd;
cd(p);
load(f);

%% calcolo dell'auto-correlazione e il vettore delle tau

%determino l'intervallo di campionamento e il numero di campioni, faccio il
%grafico del segnale nel tempo

dt=1/fsamp;
N=length(Dati);
t=0:dt:dt*(N-1);
Dati=Dati./sens;
figure
set(gca, 'fontsize', 14)
plot(t,Dati),
title('Accelerazione radiale','fontsize',16);
xlabel('t [s]','fontsize',16)
ylabel('[m/s^2]','fontsize',16)

%inizializzo le variabili contenti l'autocorrelazione e la corrispettiva
%tau
auto=zeros(1,N);
tau=zeros(1,N);

%calcolo l'autocorrelazione
for z=0:N-1
    
    sig1=Dati(1:N-z);
    sig2=Dati(1+z:N);
    prodotto=sig1.*sig2;
    auto(z+1)=sum(prodotto)/(N-z);
    tau(z+1)=z*dt;
    
end

auto_tot=[fliplr(auto) auto(2:end)];
tau_tot=[-fliplr(tau) tau(2:end)];

%plotto il risultato dell'autocorrelazione
figure(100)
set(gca, 'fontsize', 14)
plot(tau_tot,auto_tot)
title('Autocorrelazione','fontsize',16)
xlabel('tau [s]','fontsize',16)
ylabel('m^2/s^4','fontsize',16)
grid on
hold on


%% autocorrelazione con funzione xcorr


coefauto=xcorr(Dati,'biased');
coefautou=xcorr(Dati,'unbiased');
tau=[-fliplr(t),t(2:length(t))];

figure
set(gca, 'fontsize', 14)
plot(tau,coefauto),title('Autocorrelazione Xcorr biased','fontsize',16)
xlabel('tau [s]','fontsize',16)
ylabel('m^2/s^4','fontsize',16)
grid on
hold on


figure
set(gca, 'fontsize', 14)
plot(tau,coefautou),title('Autocorrelazione Xcorr unbiased','fontsize',16)
xlabel('tau [s]','fontsize',16)
ylabel('m^2/s^4','fontsize',16)
grid on
hold on


%% Stima della velocit�

%determino la posizione e il valore dei primi dieci picchi dell'autocorrelazione
[loc,val] = pickpeak(auto(1:end/2),10,500);



%plotto la posizione dei picchi sul grafico dell'autocorrelazione
figure(100)
hold on
plot((loc-1)*dt,val,'*r')

%raggio ruota in metri (diametro 16 inches)
r=16/2*0.0254;

%calcolo la velocit� di rotazione
periodi=diff(loc)*dt;
periodo=loc(2)-loc(1);
omega=2*pi/(periodo*dt);
v=omega*r;
%velocit� in km/h
v=v*3.6;  

%mostro a display la velocit�
disp(['la velocit� �: ' num2str(v) ' km/h']);


%% Time - averaging

% %determino la posizione e il valore dei picchi dell'autocorrelazione
% [loc,val] = pickpeak(auto(1:end/4),3,300);
% 
% periodo=loc(2)-loc(1);
        
%suddivido il segnale in sottostorie lunghe un periodo 

storia=0;
sig=[];

while (storia+1)*periodo<=length(Dati)

       sig=[sig Dati(1+(periodo*storia):periodo+(periodo*storia))]; 
       storia=storia+1;
       
end

%faccio la media
medie=mean(sig,2);

figure
set(gca, 'fontsize', 14)
hold on
grid on
plot(t(1:periodo),medie,'b',t(1:periodo),sig,'r')
legend('Time Averaging','Segnale originale')
title('Singolo periodo','fontsize',16)
xlabel('t [s]','fontsize',16)
ylabel('m/s^2','fontsize',16)
plot(t(1:periodo),medie,'b','linewidth',2)
xlim([0 0.15]);


    
