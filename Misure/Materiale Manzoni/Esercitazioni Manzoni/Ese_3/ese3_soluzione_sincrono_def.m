clear all
close all
clc

%% Carico il file

[nome cartella]=uigetfile();
load([cartella nome])

%% Asse delle ascisse e taratura

proxi=proxi_sinc/sens;
pt_giro=72; 

ang_rate=2*pi/pt_giro;
pos_ang=[0:length(proxi)-1]*ang_rate;

%% Plotto i segnali

figure
set(gca, 'fontsize', 18)
plot(pos_ang,proxi)
grid on
ylabel('spost [mm]','fontsize',16)
xlabel('posizione angolare [rad]','fontsize',16)
axis tight

%% Time averaging

% # periodi su cui mediare

n_periodi=floor(length(proxi)/pt_giro);

pos_ang1p=pos_ang(1:pt_giro);
somma=0;
media=zeros(1,n_periodi);

figure(100)
hold on

for tt=1:n_periodi
    
    media(tt)=mean(proxi((tt-1)*pt_giro+1:tt*pt_giro));
    somma=somma+proxi((tt-1)*pt_giro+1:tt*pt_giro)-media(tt);
    plot(pos_ang1p,proxi((tt-1)*pt_giro+1:tt*pt_giro)-media(tt))
    
end

% Time averaging
somma=somma/n_periodi;

figure
hold on
grid on
set(gca, 'fontsize', 18)
plot(1:n_periodi,media)
ylabel('media [mm]','fontsize',16)
xlabel('periodi','fontsize',16)

figure(100)
set(gca, 'fontsize', 18)
hold on
grid on
plot(pos_ang1p,somma,'r','linewidth',2)
ylabel('spost [mm]','fontsize',16)
xlabel('posizione angolare [rad]','fontsize',16)
axis tight


