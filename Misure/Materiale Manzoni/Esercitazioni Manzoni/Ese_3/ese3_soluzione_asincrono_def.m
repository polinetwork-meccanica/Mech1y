clear all
close all
clc

%% Carico il file

[nome cartella]=uigetfile();
load([cartella nome])

%% Asse dei tempi e taratura

time=[1:length(proxi)]/fsamp;
proxi=proxi/sens;

%% Determinazione del numero di punti su cui mediare

%Plotto i segnali

figure
set(gca, 'fontsize', 18)
plot(time,proxi)
grid on
ylabel('spost [mm]','fontsize',16)
xlabel('tempo [s]','fontsize',16)
axis tight

figure
set(gca, 'fontsize', 18)
plot(time,ref./sens)
grid on
ylabel('riferimento','fontsize',16)
xlabel('tempo [s]','fontsize',16)
axis tight

pause

% trovo i picchi

pos_picchi=find(ref>16);
Periodo=diff(time(pos_picchi));
hold on
plot(time(pos_picchi),ref(pos_picchi)./sens,'ro')
axis tight

% determino il # punti del primo periodo

npt_per=pos_picchi(2)-pos_picchi(1);

pause

% Plotto i primi 15 periodi per vedere se si ripetono uguali

figure
set(gca, 'fontsize', 18)
hold on
grid on
colors=['r','b','g','r','b','g','r','b','g','r','b','g','r','b','g'];

for tt=1:15

    plot((tt-1)*npt_per+1:tt*npt_per,proxi((tt-1)*npt_per+1:tt*npt_per),colors(tt))
    axis tight
    pause
    
end

%% Time averaging

% tempo corrispondende al primo periodo

time1p=time(1:npt_per);

somma=0;

% # periodi su cui mediare
n_periodi=floor(length(proxi)/npt_per);

media=zeros(1,n_periodi);

for tt=1:n_periodi
    
    media(tt)=mean(proxi((tt-1)*npt_per+1:tt*npt_per));
    somma=somma+proxi((tt-1)*npt_per+1:tt*npt_per)-media(tt);
   
end

% time averaging
somma=somma/n_periodi;

% figure dei risultati

figure
set(gca, 'fontsize', 18)
hold on
grid on
plot(1:n_periodi,media)
xlabel('periodi','fontsize',16)
ylabel('media sul periodo [mm]','fontsize',16)
axis tight

figure
set(gca, 'fontsize', 18)
hold on
grid on
plot(time1p,somma,'r','linewidth',2)
ylabel('spost [mm]','fontsize',16)
xlabel('tempo [s]','fontsize',16)
plot(time1p,proxi(1:npt_per)-media(1))
legend('Time averaging','Primo periodo')
