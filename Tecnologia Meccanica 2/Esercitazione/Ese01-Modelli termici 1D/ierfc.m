function [out] = ierfc(x)

out = -erfc(x).*x + exp(-x.^2)/(pi^0.5);

