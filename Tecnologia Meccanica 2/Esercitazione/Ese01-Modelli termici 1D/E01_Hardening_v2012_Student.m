%% Thermal models � extended source 1D

clc
clear all
close all

%%  PROBLEM
% The surface of a metallic workpiece must be heated up to 900�C by means
% of  a diode laser. The particular object shape doesn�t require any
% relative motion between its surface and the laser beam. Consider as
% constant the power distribution  across the laser section. A preliminary
% test is carried out setting the process parameters so that a constant
% power density of  7e6 W/m2  is reached with a total process time of  t=10 s. 
% In these conditions an incipient surface melting is observed.

% REQUESTS:
%  1) Calculate the inward heat flux;
%  2) Calculate the absorbtion coefficient;
%  3) Calculate the thickness reaching 900�C;
%  4) Calculate the total time the surface needs to cool down back to T_cool=200�C 



%%  DATA:
% MATERIAL PROPERTIES
% Manual parameter input
prompt = {'\rho [kg/m^3]:','Cp [J/kg K]:','k [W/m K]:','Melting temperature T_{f} [�C]:','Austenitizing temperature T_{aus} [�C]'};
dlg_title = 'Material properties';
num_lines = 1;
def = {'0','0','0','0','0'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

rho=str2num(answer{1});    %[kg/m3]
Cp=str2num(answer{2});     %[J/kg K]
k=str2num(answer{3});      %[W/m K]
Tf=str2num(answer{4});     %[�C]
Taus=str2num(answer{5});   %[�C]


% PROCESS PARAMETERS
% Manual parameter input
prompt = {'t_{on} [s]:','q_{02} [W/m^2]:','Initial (ambient) temperature T_{i} [�C]:','Cooling check temperature T_{cool} [�C]:','Total simulated time t_{tot}=t_{on}+t_{off} [s]:','Total simulated thickness x_{depth} [mm]:'};
dlg_title = 'Process parameters';
num_lines = 1;
def = {'0','0','0','0','0','0'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);
t_on=str2num(answer{1});    %[s]
q02=str2num(answer{2});     %[W/m2]
Ti=str2num(answer{3});      %[�C]
T_cool=str2num(answer{4});  %[�C]
t_tot=str2num(answer{5});   %[s]
x_depth=str2num(answer{6})*1e-3; %[m]
% Derived parameters
alpha=k/(rho*Cp);  %[m2/s] Thermal diffusivity coefficient




% SOLUTION
disp(['SOLUTION'])

%% QUESTION (1)
% Surface temperature check:
D = (4*alpha*t_on)^0.5;
T = Ti + q02*D/k*ierfc(0/D);
disp(['  '])
disp(['  '])
disp(['Question (1)  '])
disp(['  '])
disp(['Surface temperature (A=1): Tsup=', num2str(T),' �C'])
disp(['Since there is an incipient melting, the surface must be at Tsup=1500�C. '])
disp(['So we need to recalculate the effective absorbed heat flux.'])

% Evaluation of correct q02n
q02n=(Tf-Ti)*k/(D*ierfc(0/D));
disp(['  '])
disp(['Effective absorbed heat flux: q02n=',num2str(q02n),'W/m2'])

%% QUESTION (2)
disp(['  '])
disp(['  '])
disp(['Question (2)  '])
disp(['  '])
% Evaluation of surface absorption coefficient 
A=q02n/q02;
disp(['Absorption coefficient: A=q02n/q02=',num2str(A)])
 

%%  MODELLAZIONE LASER

% Spatial discretization:
x_res=0.0001;    %[m]   % Spatial resolution
x=[0:x_res:x_depth];    % Spatial vector
xc=length(x);           % Spatial vector length

% Time discretization
t_res=0.2;      %[s]    % Time resolution
t=[0:t_res:t_on];
tr=length(t);


T=zeros(tr,xc);         %Temperature matrix initialization


% ------------------------------------------------------------------------
%  LASER ON solution
% -------------------------------------------------------------------------

% Temperature matrix filling for Laser ON
for i=1:tr
    for j=1:xc
        D=(4*alpha*t(i))^0.5;
      if D==0
         T(i,j)=Ti; 
      else
         T(i,j)=Ti+q02n*D/k*ierfc(x(j)/D);
      end
    end
end

% Graphs generation
gt=20;      % Time Number Graphs
gx=20;      % Space Number Graphs

deltat = floor(tr/gt);
deltax = floor(xc/gx);

for i=1:deltat:tr
        figure(1);
        title('Time Lines LASER ON');
        xlabel('Distance from surface [m]');
        ylabel('Temperature [�C]');
        hold on;
        grid on;
        plot(x,T(i,:));
end

for i=1:deltax:xc
        figure(2);
        title('Space Lines LASER ON');
        xlabel('Time [s]');
        ylabel('Temperature [�C]');
        hold on;
        grid on;
        plot(t,T(:,i));
end


%%  QUESTION (3)
disp(['  '])
disp(['  '])
disp(['Question (3):  '])
disp(['  '])
% Hardening depth evaluation
x_900 = 0;
for i=1:xc
      if (T(tr,i)>Taus)
          x_900 = x(i);
      end
end

disp(['Hardening Depth : x_900=',num2str(x_900*1000),' mm']);

% Thermal Distance %
D = (4*alpha*t_on)^0.5;
disp(['  '])
disp(['Model applicability verification:'])
if 2.5*D < x_depth
    disp(['The thermal distance is ',num2str(D*1000),' mm, which is <<',num2str(x_depth*1000),' mm.']);
    disp(['The model hypothesis are verified!'])
else disp('WARNING!')
     disp(['The model hypothesis are not verified; increase x_depth!'])
end


%% --------------------------------------------------------------------------
%  LASER OFF solution
% --------------------------------------------------------------------------

% Spatial discretization
x_depth=0.1;     %[m]
x_res=0.0001;    %[m]

x=[0:x_res:x_depth];
xc=length(x);

% Time discretization
t_res=0.2;      %[s]

t=[0:t_res:t_tot];
tr=length(t);

for i=(t_on/t_res+2):tr     % Start from the first instant after the LASER ON solution
    for j=1:xc
        D = (4*alpha*t(i))^0.5;
        DD = (4*alpha*(t(i)-t_on))^0.5;
        T(i,j) = Ti + (q02n/k)*(D*ierfc(x(j)/D)- DD*ierfc(x(j)/DD));
    end
end

% Calculate the total time the surface needs to cool down back to T_cool=200�C
t_200 = 0;
for i=1:tr
      if (T(i,1)>T_cool)
          t_200 = t(i);
      end
end

disp(['  '])
disp(['  '])
disp(['Question (4):  '])
disp(['  '])
if t_200 < t(tr)
        disp(['Total time to cool down the surface back to 200�C: t_tot=',num2str(t_200),' s']);
else    disp('WARNING!')
        disp(['Surface temperature still higher than ',num2str(T_cool),'�C.']);
        disp('Rise up the total simulated time t_tot!');
end

%% Graphs Laser OFF
gt = 20;    % Time Number Graphs
gx = 20;    % Space Number Graphs

deltat = floor(tr/gt);
deltax = floor(xc/gx);

for i=(t_on/t_res+2):deltat:tr
    figure(3);
    title('Time Lines LASER OFF');
    xlabel('Distance from surface [m]');
    ylabel('Temperature [�C]');
    hold on;
    grid on;
    plot(x,T(i,:));
end

for i=1:deltax:xc
figure(4);
title('Space Lines LASER OFF');
xlabel('Time [s]');
ylabel('Temperature [�C]');
hold on;
grid on;
plot(t,T(:,i));
end