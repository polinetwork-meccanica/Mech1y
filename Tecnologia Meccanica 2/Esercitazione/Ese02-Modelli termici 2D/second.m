clear all;
close all;
clc;

%% Data %
% Manual parameter input
prompt = {'\rho [kg/m^3]:','Cp [J/kg K]:','k [W/m K]:','Melting temperature T_{f} [�C]:','Austenitizing temperature T_{aus} [�C]','Ambient temperature T_{i} [�C]'};
dlg_title = 'Material properties';
num_lines = 1;
def = {'7854','434','60.5','1553','930','25'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

rho=str2num(answer{1});    %[kg/m3]
cp=str2num(answer{2});     %[J/kg K]
k=str2num(answer{3});      %[W/m K]
Tf=str2num(answer{4});     %[�C]
Taus=str2num(answer{5});   %[�C]
Ti=str2num(answer{6});     %[�C]

% Manual parameter input
prompt = {'Power [W]:','Sheet thickness [mm]:','Feed rate v [mm/s]:','Cutting length L [mm]:','Spatial discretization [mm]:','Time discretization [s]:'};
dlg_title = 'Process parameters and discretization settings';
num_lines = 1;
def = {'4e3','6','30','300','0.1','0.1'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

Q=str2num(answer{1});       %[W]
s=str2num(answer{2})*1e-3;  %[mm]
v=str2num(answer{3})*1e-3;  %[m/s]
L=str2num(answer{4});       %[�C]
step_x=str2num(answer{5});  %[mm]
step_y = step_x;            %[mm]
step_t=str2num(answer{6});  %[mm]

% Derived parameters
alpha = k/(rho*cp);
lambda = 1/(2*alpha);

q1 = Q/s;           %[W/m] 

%% Domain discretization %

x = [-10:step_x:L]./1000;           %[m]
y = [-10:step_y:10]./1000;          %[m]

tau = (L/1000)/v;                   % Total process time [s]
t = [0:step_t:tau];                 % Time vector

%% Evolution in time of the temperature distribution along y direction (x fixed)

x_rif = (L/2)/1000;    %[m] Reference point coordinate

const = q1/(2*pi*k);
for w=1:length(t)
    tmp = x_rif-v*t(w);
    for j=1:length(y)
        tmptmp = sqrt(tmp^2+y(j)^2);
       	T(w,j) = Ti + const*exp(-lambda*v*tmp)*besselk(0,v*tmptmp*lambda);
    end
end

%% Plots

a = round((x_rif/v)/step_t);    % General expression of the start time index to plot
b = a+10;                       % End time index to plot  

figure(1);
for w=a:b
    title('Fronte di Temperatura in X costante');
    hold on;
    axis on;
    grid on;
    xlabel('Distanza dalla sorgente lungo Y [m]');
    ylabel('Temperatura [�C]');
    plot(y,T(w,:));
end


%% Temperature evolution in a specific point of coordinate (x_sing,y_sing) %
% Manual parameter input
prompt = {'x coordinate [mm]:','y coordinate [mm]:'};
dlg_title = 'Coordinates of the control point';
num_lines = 1;
def = {'150','0.1'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

x_sing=str2num(answer{1})*1e-3;  %[mm]
y_sing=str2num(answer{2})*1e-3;  %[mm]


clear T;
for w=1:length(t)
    tmp = x_sing-v*t(w);
    tmptmp = sqrt(tmp^2+y_sing^2);
    T(w) = Ti + const*exp(-lambda*v*tmp)*besselk(0,v*tmptmp*lambda);
end

for w=1:length(t)-1                 %Mi fermo un istante prima
    dT(w) = ((T(w+1)-T(w))/step_t);
end

figure(2);
subplot(1,2,1);
plot(t,T);
title('Temperature evolution in the specific point');
axis on;
xlabel('tempo [s]');
ylabel('Temperatura [�C]');
grid on;
subplot(1,2,2);
plot(t(1:length(t)-1),dT);  % 
title('Temperature derivative in the specific point');
axis on;
xlabel('tempo [s]');
ylabel('dT/dt');
grid on;