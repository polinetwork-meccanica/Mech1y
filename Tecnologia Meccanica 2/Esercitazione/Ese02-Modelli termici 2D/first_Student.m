%% ESERCITAZIONE 2 - MODELLI TERMICI - SORGENTE IN MOVIMENTO - FILE 1
clear all;
close all;
clc;

%% DATA
% Manual parameter input
prompt = {'\rho [kg/m^3]:','Cp [J/kg K]:','k [W/m K]:','Melting temperature T_{f} [�C]:','Austenitizing temperature T_{aus} [�C]','Ambient temperature T_{i} [�C]'};
dlg_title = 'Material properties';
num_lines = 1;
def = {'0','0','0','0','0','0'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

rho=str2num(answer{1});    %[kg/m3]
cp=str2num(answer{2});     %[J/kg K]
k=str2num(answer{3});      %[W/m K]
Tf=str2num(answer{4});     %[�C]
Taus=str2num(answer{5});   %[�C]
Ti=str2num(answer{6});     %[�C]

% PROCESS and CONTROL PARAMETERS
% Manual parameter input
prompt = {'Power [W]:','Sheet thickness [mm]:','Feed rate v [mm/s]:','Maximum temperature to plot T_{max} [�C]:','Minimum temperature to plot T_{min} [�C]:','Number of isotherm to plot:'};
dlg_title = 'Process and control parameter';
num_lines = 1;
def = {'0','0','0','0','0','2'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);

Q=str2num(answer{1});       %[W]
l=str2num(answer{2})*1e-3;  %[mm]
v=str2num(answer{3})*1e-3;  %[m/s]
Tisoterma_Max=str2num(answer{4});  %[�C]
Tisoterma_Min=str2num(answer{5});  %[�C]
N_isoterme=str2num(answer{6});     %[-]

% Manual parameter input
prompt = {'x_{min} [mm]:','x_{max} [mm]:','y_{min} [mm]:','y_{max} [mm]:','Spatial discretization [mm]:'};
dlg_title = 'Domain iscretization';
num_lines = 1;
def = {'-38','2','-20','20','0.2'};
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
answer = inputdlg(prompt,dlg_title,num_lines,def,options);
xmin=str2num(answer{1});          %[mm]
xmax=str2num(answer{2});          %[mm]
ymin=str2num(answer{3});          %[mm]
ymax=str2num(answer{4});          %[mm]
passo=str2num(answer{5});          %[mm]

% Derived parameters
alpha = k/(rho*cp);
lambda = 1/(2*alpha);

q1 = Q/l;           %[W/m]


%% Discretization 

xi = [xmin:passo:xmax]./1000;       %NB: the variable csi(i) is called here xi 
y = [ymin:passo:ymax]./1000;

%% Stationary case %

Txi = zeros(length(xi),length(y));          % Temperature matrix initialization
[XI,Y] = meshgrid(y,xi);                    % xi and y are switched as a convention in the meshgrid command

const = q1/(2*pi*k);
for i=1:length(xi)
    for j=1:length(y)
        tmp = sqrt(xi(i)^2+y(j)^2);
        Txi(i,j) = Ti + const*exp(-lambda*v*xi(i))*besselk(0,v*tmp*lambda); % Stationary solution
    end
end

%% STATIONARY SOLUTION
disp('RESULTS')
disp('  ')

% 1. 3D plot
figure(1);
subplot(1,2,1);
mesh(XI,Y,Txi);
grid on;
title('Soluzione Stazionaria 3D');
xlabel('y [m]');
ylabel('csi [m]');

% 2. Plot of the isotherm contour
subplot(1,2,2);
[C,h] = contour(XI,Y,Txi);
grid on;
title('Soluzione Stazionaria isoterme');
xlabel('y [m]');
ylabel('csi [m]');
colorbar

% 3. 3D plot limited to the melting temperature Tf
T_ref = Tf;
for i=1:length(xi)
    for j=1:length(y)
        if (Txi(i,j)>T_ref)
            Txi(i,j) = T_ref;
        end
    end
end
figure(2);
subplot(1,2,1);
mesh(XI,Y,Txi);
grid on;
title('Soluzione Stazionaria limitata in Temperatura 3D');
xlabel('y [m]');
ylabel('csi [m]');
subplot(1,2,2);
[C,h] = contour(XI,Y,Txi);
grid on;
title('Soluzione Stazionaria limitata in Temperatura isoterme');
xlabel('y [m]');
ylabel('csi [m]');
colorbar

%% Analysis of the isotherm contours chosen by the user

% 1. Plot of the chosen isotherm contours
T_isoterme=[Tisoterma_Min: (Tisoterma_Max-Tisoterma_Min)/(N_isoterme-1): Tisoterma_Max];
figure(3);
contour(XI,Y,Txi, T_isoterme);
title('Soluzione Stazionaria isoterme richieste');
grid on;
xlabel('y [m]');
ylabel('csi [m]');
colorbar;
hold on;


% 2. Evaluation of the chosen isotherms width
isoterma = [];
for k=1:N_isoterme
    
a=1;
for i=1:length(xi)
    for j=1:length(y)
        tmp = sqrt(xi(i)^2+y(j)^2);
        Txi(i,j) = Ti + const*exp(-lambda*v*xi(i))*besselk(0,v*tmp*lambda);
        if (Txi(i,j)>=T_isoterme(k)-5&&Txi(i,j)<=T_isoterme(k)+5)
            isoterma(a,1)=i;
            isoterma(a,2)=j;
            a=a+1;
        end
    end
end


A=min(isoterma(:,2));
B=max(isoterma(:,2));
larghezza = (B-A)*passo;


disp('  ')
disp(['The width of the isotherm contour at ', num2str(T_isoterme(k)), ' �C is ', num2str(larghezza), ' mm' ])


clear isoterma


end





