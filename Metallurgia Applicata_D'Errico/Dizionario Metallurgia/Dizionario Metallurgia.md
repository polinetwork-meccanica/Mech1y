* **Ductility**: solid material's ability to deform under tensile stress.
* **Malleability**: material's ability to deform under compressive stress.
* **DBTT** (*ductile brittle transition temperature*): point at which the fracture energy passes below a pre-determined point.
* **Plasticity**: extent to which a solid material can be plastically deformed without fracture
* **Yield**: stress at which a material begins to deform plastically.
* **Tensile Strength**: maximum stress that a material can withstand while being stretched or pulled before failing or breaking (≠ compressive strength).
* **Toughness**: ability of a material to absorb energy and plastically deform without fracturing.
* **Strength** (resistenza meccanica): ability to withstand an applied stress without failure.
* **Strain-hardening**: 
* **Mild steels** (acciaio non legato)
* **Quenching** (tempra)
* **Annealing** (ricottura)