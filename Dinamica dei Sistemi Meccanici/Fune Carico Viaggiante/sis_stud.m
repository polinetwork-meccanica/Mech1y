function ypi=sis(ti,yi)

global A
global C
global u
global v
global gamma
global tmax

if ti<tmax
    phiP=[];

    Q1=-phiP;
else
    Q1=zeros(5,1);
end
Q=[Q1;zeros(size(Q1))];
B=inv(C)*Q;

ui=u;

ypi=A*yi+B*ui;
