clear all
close all


[file_i,xy,nnod,sizew,idb,ngdl,incidenze,l,gamma,m,EA,EJ,posiz,nbeam]=loadstructure


% Assemblo
[M,K]=assem(incidenze,l,m,EA,EJ,gamma,idb);

% Plottaggio struttura indeformata
dis_stru(posiz,l,gamma,xy)

% Calcolare freq. proprie e modi di vibrare

% Plottaggio modo di vibrare
diseg2(modi,fscala,incidenze,l,gamma,posiz,idb,xy);


