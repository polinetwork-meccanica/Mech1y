% Esercizio fatto a mano il 10/3/2014


m  = 1000;
k = 1000; % N/m
A = 1;
J = 2*m*L^2;
L = 2;

l1 = L;
l2 = 2*L;

M = [m 0;
     0 J];

K = [2*k           k*(l2 - l1);
     k*(l2 - l1)   k*(l1^2 + l2^2)];

% La matrice su cui calcolare gli omega^2 ? (M^-1)*K
[autoval, omega2] = eig((M^-1)*K);

omega = diag(omega2.^0.5)

% Normalizzo gli autovettori in modo da averne uno uguale a (meno) 1
autoval_adim = [autoval(:, 1)./(max(abs(autoval(:, 1)))) autoval(:, 2)./(max(abs(autoval(:, 2))))]

% Introduciamo anche degli smorzatori, pari a:
R = 0.001.*K;

% Forza nel baricentro e nessuna coppia:
%F = [1 0]'; % F sarebbe F * e^(i*omega*t)
F = [1, -L]';
X = [];

for om = omegaF
    A = -(om^2).*M + i*om.*R + K;
    x0 = inv(A)*F;
    X = [X; x0'];
end

figure()
title('|Z|');
plot(omegaF, abs(X(:, 1)))
%hold on
%line([omega(1) omega(1)], [min(abs(X(:, 1))) max(abs(X(:, 1)))*1.5])
figure()
title('Fase Z');
plot(omegaF, -angle(X(:, 1)))
figure()
title('|theta|');
plot(omegaF, abs(X(:, 2)))
figure()
title('Fase theta');



% mettiamo ora la forza nell'estremo sinistro anzich? nel baricentro
% Per farlo bisogna scrivere la solita tabella che lega i delta_z e
% delta_theta con lo spostamento del punto dove sto applicando la forza.
% Risulta che il vettore F ?
F = [1, -L]';
plot(omegaF, -angle(X(:, 2)))
% Si vede che il secondo modo di vibrare non interviene, alternativamente
% si poteva calcolare la componente lagrangiana della forza e si vede che
% il secondo modo non lavora.
% Se spostassimo la forzante sull'estremo di destra si vedrebbe, al
% contrario, sparire il primo modo



% Per risolvere l'esercizio fatto su carta oggi:
m = 100
k = 100
M = [m 0 0; 0 m 0; 0 0 m];
K = [2*k  -k 0; -k 2*k -k; 0 -k 2*k];

